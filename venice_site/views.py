from django.shortcuts import render

# Create your views here.


def home(request):
    return render(request, 'venice_site/site.html', {})
