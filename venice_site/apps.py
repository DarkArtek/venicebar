from django.apps import AppConfig


class VeniceSiteConfig(AppConfig):
    name = 'venice_site'
